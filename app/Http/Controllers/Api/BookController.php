<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\BookResource;
use App\Models\Book;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class BookController extends Controller
{
    public function index()
    {
        $books = Book::latest()->get();

        return new BookResource(true, 'List Data Buku', $books);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'required|image|mimes:png,jpg,jpeg,gif,svg|max:2048',
            'title' => 'required',
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $image = $request->file('image')->store('images', 'public');

        $book = Book::create([
            'image' => $image,
            'title' => $request->input('title'),
            'description' => $request->input('description')
        ]);

        return new BookResource(true, 'Data Buku Berhasil Ditambahkan!', $book);
    }

    public function show(Book $book)
    {
        // try {
        //     throw new Exception('coba');
        // } catch (Exception $e) {
        //     return response()->json([
        //         'message' => $e->getMessage()
        //     ]);
        // }
        // return new BookResource(true, 'Data Buku Berhasil Ditemukan', $book);
    }

    public function update(Request $request, Book $book)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $data = [
            'title' => $request->input('title'),
            'description' => $request->input('description')
        ];

        if ($request->hasFile('image')) {
            $image = $request->file('image')->store('images', 'public');
            $data['image'] = $image;

            Storage::delete("public/$book->image");

            $book->update($data);
        } else {
            $book->update($data);
        }

        return new BookResource(true, 'Data Buku Berhasil Diubah', $book);
    }

    public function destroy(Book $book)
    {
        Storage::delete("public/$book->image");

        $book->delete();

        return new BookResource(true, 'Data Buku Berhasil Dihapus', null);
    }
}
